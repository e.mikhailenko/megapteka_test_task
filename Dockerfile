FROM node:18.10.0-alpine as builder
WORKDIR /app

RUN yarn global add @nestjs/cli
COPY package.json yarn.lock ./

RUN yarn install
COPY . .

RUN nest build api

FROM node:18.10.0-alpine as executor
WORKDIR /app
COPY --from=builder /app .

