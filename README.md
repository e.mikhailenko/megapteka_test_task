
## Description
Test task for megapteka

## Config

```bash
/config/local.yml
```

## Swagger

```bash
localhost:3000/api/v1/docs
```

## Installation

```bash
$ docker-compose up or yarn
```

## Running the app

```bash
# local without docker
$ yarn start
```
