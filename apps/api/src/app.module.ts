import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { initialDbConfiguration } from '@app/config';
import { GroupEntity, ProductEntity } from '@app/entities';
import { ProductModule } from './product/product.module';
import { GroupModule } from './group/group.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(initialDbConfiguration),
    TypeOrmModule.forFeature([GroupEntity, ProductEntity]),
    GroupModule,
    ProductModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {
}
