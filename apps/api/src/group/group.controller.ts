import { ApiBadRequestResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { GroupService } from './group.service';
import {
  CreateGroupDto,
  CreateGroupResponseDto,
  DeleteGroupResponseDto,
  FindAllGroupsResponseDto,
  FindOneGroupResponseDto,
  UpdateGroupDto,
  UpdateGroupResponseDto,
} from './dto/group.dto';
import { ErrorResponse } from '@app/common';

@ApiTags('groups')
@Controller('groups')
@ApiBadRequestResponse({ description: 'groups service error' })
export class GroupController {
  constructor(
    private groupService: GroupService,
  ) {
  }

  @Post()
  @ApiOperation({ description: 'create group' })
  @ApiOkResponse({ description: 'create group success', type: CreateGroupResponseDto })
  @ApiBadRequestResponse({ description: 'create group error', type: ErrorResponse })
  async create(@Body() createUserDto: CreateGroupDto): Promise<CreateGroupResponseDto | BadRequestException> {
    return await this.groupService.create(createUserDto);
  }

  @Get()
  @ApiOperation({ description: 'find all groups' })
  @ApiOkResponse({ description: 'find all groups success', type: FindAllGroupsResponseDto })
  @ApiBadRequestResponse({ description: 'find all groups error', type: ErrorResponse })
  async findAll(): Promise<FindAllGroupsResponseDto | BadRequestException> {
    return await this.groupService.findAll();
  }

  @Get(':uuid')
  @ApiOperation({ description: 'find one group' })
  @ApiOkResponse({ description: 'find one group success', type: FindOneGroupResponseDto })
  @ApiBadRequestResponse({ description: 'find one group error', type: ErrorResponse })
  async findOne(@Param('uuid') uuid: string): Promise<FindOneGroupResponseDto | BadRequestException> {
    return await this.groupService.findOne(uuid);
  }

  @Patch(':uuid')
  @ApiOperation({ description: 'update group' })
  @ApiOkResponse({ description: 'update group success', type: UpdateGroupResponseDto })
  @ApiBadRequestResponse({ description: 'update group error', type: ErrorResponse })
  async update(@Param('uuid') uuid: string, @Body() body: UpdateGroupDto): Promise<UpdateGroupResponseDto | BadRequestException> {
    return await this.groupService.update(uuid, body);
  }

  @Delete(':uuid')
  @ApiOperation({ description: 'delete group' })
  @ApiOkResponse({ description: 'delete group success', type: DeleteGroupResponseDto })
  @ApiBadRequestResponse({ description: 'delete error', type: ErrorResponse })
  async delete(@Param('uuid') uuid: string): Promise<DeleteGroupResponseDto | BadRequestException> {
    return await this.groupService.delete(uuid);
  }
}