import { ApiProperty } from '@nestjs/swagger';
import { GroupEntity } from '@app/entities';

export class GroupDto {
  @ApiProperty({ description: 'group name' })
  readonly name: string;

  @ApiProperty({ description: 'group description' })
  readonly description: string;

  @ApiProperty({ description: 'group parent uuid' })
  readonly parentUUID?: string;
}

export class CreateGroupDto extends GroupDto {
}

export class CreateGroupResponseDto {
  @ApiProperty({ description: 'new group', type: GroupDto })
  readonly newGroup: GroupEntity;
}

export class FindAllGroupsResponseDto {
  @ApiProperty({ description: 'groups list', type: [GroupDto] })
  readonly groups: GroupEntity[];
}

export class FindOneGroupResponseDto {
  @ApiProperty({ description: 'found group', type: GroupDto })
  readonly group: GroupEntity;
}

export class UpdateGroupDto extends GroupDto {
}

export class UpdateGroupResponseDto {
  @ApiProperty({ description: 'updated group', type: GroupDto })
  readonly updatedGroup: GroupEntity;
}

export class DeleteGroupResponseDto {
  @ApiProperty({ description: 'deleted group', type: GroupDto })
  readonly deletedGroup: GroupEntity;
}

