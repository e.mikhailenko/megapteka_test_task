import { BadRequestException, Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { GroupDAOService } from '@app/dao';
import {
  CreateGroupDto,
  CreateGroupResponseDto,
  DeleteGroupResponseDto,
  FindAllGroupsResponseDto,
  FindOneGroupResponseDto,
  UpdateGroupDto,
  UpdateGroupResponseDto,
} from './dto/group.dto';

@Injectable()
export class GroupService implements OnApplicationBootstrap {
  private readonly logger = new Logger(GroupService.name, { timestamp: true });

  constructor(
    private readonly groupDAOService: GroupDAOService,
  ) {
  }

  async onApplicationBootstrap(): Promise<void> {
    await this.groupsInit();
  }

  async groupsInit(): Promise<void> {
    const testGroup1 = await this.groupDAOService.findOne({ where: { name: 'test_group_1' } });
    if (!testGroup1) {
      const newTestGroup1 = await this.groupDAOService.insert({ name: 'test_group_1', parentUUID: null });
      const testGroup2 = await this.groupDAOService.findOne({ where: { name: 'test_group_2' } });
      if (!testGroup2) await this.groupDAOService.insert({ name: 'test_group_2', parentUUID: newTestGroup1.raw[0].uuid });
    }

    const testGroup2 = await this.groupDAOService.findOne({ where: { name: 'test_group_2' } });
    if (!testGroup2) await this.groupDAOService.insert({ name: 'test_group_2', parentUUID: testGroup1.uuid });
  }

  async findAll(): Promise<FindAllGroupsResponseDto | BadRequestException> {
    try {
      this.logger.log('find all groups start');
      const groups = await this.groupDAOService.find({});
      return { groups: groups[0] };
    } catch (error) {
      return new BadRequestException(error.message);
    }
  }

  async findOne(uuid: string): Promise<FindOneGroupResponseDto | BadRequestException> {
    try {
      this.logger.log('find one group start');
      const group = await this.groupDAOService.findOne({ where: { uuid } });
      return { group };
    } catch (error) {
      return new BadRequestException(error.message);
    }
  }

  async create(body: CreateGroupDto): Promise<CreateGroupResponseDto | BadRequestException> {
    try {
      this.logger.log('create group start');
      const newGroup = await this.groupDAOService.insert(body);
      return { newGroup: newGroup.raw[0] };
    } catch (error) {
      return new BadRequestException(error.message);
    }
  }

  async update(uuid: string, body: UpdateGroupDto): Promise<UpdateGroupResponseDto | BadRequestException> {
    try {
      const updatedGroup = await this.groupDAOService.update({ uuid }, body);
      return { updatedGroup: updatedGroup.raw[0] };
    } catch (error) {
      return new BadRequestException(error.message);
    }
  }

  async delete(uuid: string): Promise<DeleteGroupResponseDto | BadRequestException> {
    try {
      this.logger.log('delete group start');
      const deletedGroup = await this.groupDAOService.delete({ where: { uuid } });
      return { deletedGroup: deletedGroup.raw[0] };
    } catch (error) {
      return new BadRequestException(error.message);
    }
  }
}