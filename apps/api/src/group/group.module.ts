import { Module } from '@nestjs/common';
import { GroupDAOModule } from '@app/dao';
import { GroupController } from './group.controller';
import { GroupService } from './group.service';

@Module({
  imports: [GroupDAOModule],
  controllers: [GroupController],
  providers: [GroupService],
})
export class GroupModule {
}