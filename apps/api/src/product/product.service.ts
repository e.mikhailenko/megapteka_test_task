import { BadRequestException, Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { GroupDAOService, ProductDAOService } from '@app/dao';
import {
  CreateProductDto,
  CreateProductResponseDto,
  DeleteProductResponseDto,
  FindAllProductsResponseDto,
  FindOneProductResponseDto,
  UpdateProductDto,
  UpdateProductResponseDto,
} from './dto/product.dto';

@Injectable()
export class ProductService implements OnApplicationBootstrap {
  private readonly logger = new Logger(ProductService.name, { timestamp: true });

  constructor(
    private readonly productDAOService: ProductDAOService,
    private readonly groupDAOService: GroupDAOService,
  ) {
  }

  async onApplicationBootstrap(): Promise<void> {
    setTimeout(async () => {
      await this.productsInit();
    }, 1000);
  }

  async productsInit(): Promise<void> {
    const testGroup2 = await this.groupDAOService.findOne({ where: { name: 'test_group_2' } });
    const testProduct1 = await this.productDAOService.findOne({ where: { name: 'test_product_1' } });
    if (!testProduct1) {
      await this.productDAOService.save({
        name: 'test_product_1',
        description: 'test_description_1',
        manufacturer: 'test_manufacturer_1',
        groups: [testGroup2],
      });
    }
  }


  async findAll(): Promise<FindAllProductsResponseDto | BadRequestException> {
    try {
      this.logger.log('find all products start');
      const products = await this.productDAOService.find({});
      return { products: products[0] };
    } catch (error) {
      return new BadRequestException(error.message);
    }
  }

  async findOne(uuid: string): Promise<FindOneProductResponseDto | BadRequestException> {
    try {
      this.logger.log('find one product start');
      const product = await this.productDAOService.findOne({ where: { uuid } });
      return { product };
    } catch (error) {
      return new BadRequestException(error.message);
    }
  }

  async create(body: CreateProductDto): Promise<CreateProductResponseDto | BadRequestException> {
    try {
      this.logger.log('create product start');
      const group = await this.groupDAOService.findOne({ where: { uuid: body.groupUUID } });
      if (!group) return new BadRequestException({ message: 'There is no such group with this uuid' });

      const childGroups = await this.groupDAOService.find({ where: { parentUUID: group.uuid } });
      if (childGroups[1] > 0) return new BadRequestException({ message: 'Group must has not any child groups' });

      const newProduct = await this.productDAOService.save({ ...body, groups: [group] });
      return { newProduct: newProduct.raw[0] };
    } catch (error) {
      return new BadRequestException(error.message);
    }
  }

  async update(uuid: string, body: UpdateProductDto): Promise<UpdateProductResponseDto | BadRequestException> {
    try {
      this.logger.log('update product start');
      const group = await this.groupDAOService.findOne({ where: { uuid: body.groupUUID } });
      if (!group) return new BadRequestException({ message: 'There is no such group with this uuid' });

      const childGroups = await this.groupDAOService.find({ where: { parentUUID: group.uuid } });
      if (childGroups[1] > 0) return new BadRequestException({ message: 'Group must has not any child groups' });

      const updatedProduct = await this.productDAOService.update({ uuid }, { ...body, groups: [group] });
      return { updatedProduct: updatedProduct.raw[0] };
    } catch (error) {
      return new BadRequestException(error.message);
    }
  }

  async delete(uuid: string): Promise<DeleteProductResponseDto | BadRequestException> {
    try {
      this.logger.log('delete product start');
      const deletedProduct = await this.productDAOService.delete({ where: { uuid } });
      return { deletedProduct: deletedProduct.raw[0] };
    } catch (error) {
      return new BadRequestException(error.message);
    }
  }
}