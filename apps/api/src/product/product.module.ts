import { Module } from '@nestjs/common';
import { GroupDAOModule, ProductDAOModule } from '@app/dao';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';

@Module({
  imports: [
    ProductDAOModule,
    GroupDAOModule,
  ],
  controllers: [ProductController],
  providers: [ProductService],
})
export class ProductModule {
}