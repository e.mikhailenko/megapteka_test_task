import { ApiProperty } from '@nestjs/swagger';
import { ProductEntity } from '@app/entities';

export class ProductDto {
  @ApiProperty({ description: 'product name' })
  readonly name: string;

  @ApiProperty({ description: 'product description' })
  readonly description: string;

  @ApiProperty({ description: 'product manufacturer' })
  readonly manufacturer: string;

  @ApiProperty({ description: 'group uuid for product' })
  readonly groupUUID: string;
}

export class CreateProductDto extends ProductDto {
}

export class CreateProductResponseDto {
  @ApiProperty({ description: 'new product', type: ProductDto })
  readonly newProduct: ProductEntity;
}

export class FindAllProductsResponseDto {
  @ApiProperty({ description: 'products list', type: [ProductDto] })
  readonly products: ProductEntity[];
}

export class FindOneProductResponseDto {
  @ApiProperty({ description: 'found product', type: ProductDto })
  readonly product: ProductEntity;
}

export class UpdateProductDto extends ProductDto {
}

export class UpdateProductResponseDto {
  @ApiProperty({ description: 'updated product', type: ProductDto })
  readonly updatedProduct: ProductEntity;
}

export class DeleteProductResponseDto {
  @ApiProperty({ description: 'deleted product', type: ProductDto })
  readonly deletedProduct: ProductEntity;
}

