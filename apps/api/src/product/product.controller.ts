import { ApiBadRequestResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { ProductService } from './product.service';
import { ErrorResponse } from '@app/common';
import {
  CreateProductDto,
  CreateProductResponseDto,
  DeleteProductResponseDto,
  FindAllProductsResponseDto,
  FindOneProductResponseDto,
  UpdateProductDto,
  UpdateProductResponseDto,
} from './dto/product.dto';

@ApiTags('products')
@Controller('products')
@ApiBadRequestResponse({ description: 'products service error' })
export class ProductController {
  constructor(
    private productService: ProductService,
  ) {
  }

  @Post()
  @ApiOperation({ description: 'create product' })
  @ApiOkResponse({ description: 'create product success', type: CreateProductResponseDto })
  @ApiBadRequestResponse({ description: 'create product error', type: ErrorResponse })
  async create(@Body() createUserDto: CreateProductDto): Promise<CreateProductResponseDto | BadRequestException> {
    return await this.productService.create(createUserDto);
  }

  @Get()
  @ApiOperation({ description: 'find all products' })
  @ApiOkResponse({ description: 'find all products success', type: FindAllProductsResponseDto })
  @ApiBadRequestResponse({ description: 'find all products error', type: ErrorResponse })
  async findAll(): Promise<FindAllProductsResponseDto | BadRequestException> {
    return await this.productService.findAll();
  }

  @Get(':uuid')
  @ApiOperation({ description: 'find one product' })
  @ApiOkResponse({ description: 'find one product success', type: FindOneProductResponseDto })
  @ApiBadRequestResponse({ description: 'find one product error', type: ErrorResponse })
  async findOne(@Param('uuid') uuid: string): Promise<FindOneProductResponseDto | BadRequestException> {
    return await this.productService.findOne(uuid);
  }

  @Patch(':uuid')
  @ApiOperation({ description: 'update product' })
  @ApiOkResponse({ description: 'update product success', type: UpdateProductResponseDto })
  @ApiBadRequestResponse({ description: 'update product error', type: ErrorResponse })
  async update(@Param('uuid') uuid: string, @Body() body: UpdateProductDto): Promise<UpdateProductResponseDto | BadRequestException> {
    return await this.productService.update(uuid, body);
  }

  @Delete(':uuid')
  @ApiOperation({ description: 'delete product' })
  @ApiOkResponse({ description: 'delete product success', type: DeleteProductResponseDto })
  @ApiBadRequestResponse({ description: 'delete error', type: ErrorResponse })
  async delete(@Param('uuid') uuid: string): Promise<DeleteProductResponseDto | BadRequestException> {
    return await this.productService.delete(uuid);
  }
}