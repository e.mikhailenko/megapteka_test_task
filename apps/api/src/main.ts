import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import { writeFile } from 'fs/promises';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

let logger: Logger;
const globalPrefix = 'api/v1';

async function bootstrap() {
  logger = new Logger('megapteka');
  try {
    const app = await NestFactory.create(AppModule);
    app.setGlobalPrefix(globalPrefix);
    app.useGlobalPipes(new ValidationPipe());

    app.enableCors({
      origin: true,
      methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
      allowedHeaders:
        'X-Requested-With, X-HTTP-Method-Override, Content-Type,' +
        ' Accept, Observe, Access-Control-Allow-Origin',
      credentials: true,
    });

    const options = new DocumentBuilder()
      .setTitle('megapteka')
      .setDescription('Provides REST API')
      .setVersion('1.0.0')
      .addBearerAuth()
      .build();

    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup(`${globalPrefix}/docs`, app, document);
    await writeFile('stargate.swagger.json', JSON.stringify(document));
    await app.listen(3000);
    logger.debug(`Application is running on ${await app.getUrl()}`);
  } catch (error) {
    logger.error(error);
  }
}

bootstrap().catch((error) => console.error(error.message));
