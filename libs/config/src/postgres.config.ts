import { readFileSync } from 'fs';
import * as config from 'config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { IPostgresConfig } from '@app/config/interfaces';
import { GroupEntity, ProductEntity } from '@app/entities';

export const POSTGRES_CONFIG = config.get<IPostgresConfig>('postgres');

export const initialDbConfiguration: TypeOrmModuleOptions = {
  type: 'postgres',
  host: POSTGRES_CONFIG.host,
  port: POSTGRES_CONFIG.port,
  username: POSTGRES_CONFIG.username,
  password: POSTGRES_CONFIG.password,
  database: POSTGRES_CONFIG.database,
  entities: [
    ProductEntity,
    GroupEntity,
  ],
  logging: false,
  synchronize: POSTGRES_CONFIG.synchronize,
  ssl: POSTGRES_CONFIG.sslOn
    ? {
      ca: readFileSync(POSTGRES_CONFIG.ssl?.ca, 'utf-8'),
      key: POSTGRES_CONFIG.ssl?.key
        ? readFileSync(POSTGRES_CONFIG.ssl?.key, 'utf-8')
        : null,
      cert: POSTGRES_CONFIG.ssl?.cert
        ? readFileSync(POSTGRES_CONFIG.ssl?.cert, 'utf-8')
        : null,
    }
    : null,
};
