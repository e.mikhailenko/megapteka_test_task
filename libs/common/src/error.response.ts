import { ApiProperty } from '@nestjs/swagger';

export class ErrorResponse {
  @ApiProperty()
  readonly statusCode: number;

  @ApiProperty()
  readonly message: string;
}


export class OkResponse {
  @ApiProperty()
  readonly message: string;
}
