import { Module } from '@nestjs/common';
import { GroupEntity } from '@app/entities';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GroupDAOService } from '@app/dao/group/group-dao.service';

@Module({
  imports: [TypeOrmModule.forFeature([GroupEntity])],
  providers: [GroupDAOService],
  exports: [GroupDAOService],
})
export class GroupDAOModule {
}