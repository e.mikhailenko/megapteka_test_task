import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GroupEntity } from '@app/entities';
import { FindOneOptions, Repository } from 'typeorm';

@Injectable()
export class GroupDAOService {
  constructor(@InjectRepository(GroupEntity) private repository: Repository<GroupEntity>,
  ) {
  }

  async find(options) {
    return this.repository.findAndCount(options);
  }

  async findOne(options: FindOneOptions) {
    return this.repository.findOne(options);
  }

  async insert(data) {
    return this.repository.insert(data);
  }

  async upsert(data) {
    return this.repository.upsert(data, ['uuid']);
  }

  async save(data) {
    return this.repository.save(data);
  }

  async update(filter, data) {
    return this.repository.update(filter, data);
  }

  async delete(filter) {
    return this.repository.delete(filter);
  }
}