import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductEntity } from '@app/entities';
import { ProductDAOService } from '@app/dao/product/product-dao.service';

@Module({
  imports: [TypeOrmModule.forFeature([ProductEntity])],
  providers: [ProductDAOService],
  exports: [ProductDAOService],
})
export class ProductDAOModule {
}