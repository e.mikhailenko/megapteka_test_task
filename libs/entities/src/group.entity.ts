import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TableNameEnum } from '@app/entities/enums';
import { ProductEntity } from '@app/entities/product.entity';

@Entity({ name: TableNameEnum.GROUPS })
export class GroupEntity {
  @PrimaryGeneratedColumn('uuid')
  uuid: string;

  @Column({ type: 'varchar', name: 'name', nullable: false })
  name: string;

  @Column({ type: 'varchar', name: 'parent_uuid', nullable: true })
  parentUUID: string;

  @ManyToOne(() => GroupEntity, (group) => group.uuid)
  @JoinColumn({ name: 'parent_uuid' })
  parent: GroupEntity;

  @ManyToMany(() => ProductEntity, (product: ProductEntity) => product.uuid)
  products: ProductEntity[];
}
