import { TableNameEnum } from '@app/entities/enums';
import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { GroupEntity } from '@app/entities/group.entity';

@Entity({ name: TableNameEnum.PRODUCTS })
export class ProductEntity {
  @PrimaryGeneratedColumn('uuid')
  uuid: string;

  @Column({ type: 'varchar', name: 'name', nullable: false })
  name: string;

  @Column({ type: 'varchar', name: 'description', nullable: false })
  description: string;

  @Column({ type: 'varchar', name: 'manufacturer', nullable: false })
  manufacturer: string;

  @ManyToMany(() => GroupEntity, (group: GroupEntity) => group.uuid)
  @JoinTable({
    name: 'products_groups',
    joinColumn: { name: 'product_uuid', referencedColumnName: 'uuid' },
    inverseJoinColumn: { name: 'group_uuid', referencedColumnName: 'uuid' },
  })
  groups: GroupEntity[];
}
